<div class="container">
    <div class="row mb-3 mt-3">
        <div class="col-lg-6">
            <h3>Crawling Result</h3>
        </div>
        <div class="col-lg-6">
            <div class="float-right">
                <a href="<?php echo base_url();?>csv/merge/<?php echo $this->session->userdata('id');?>.csv" class="btn btn-primary" role="button">Export All as CSV</a>
                <a href="<?php echo base_url();?>Calculation" class="btn btn-warning">Next Step</a>
            </div>
        </div>
    </div>
    <div class ="row">
<?php
$no = 0;
for ($i = 0; $i<$link;$i++){
    $no++;
?>
<div class="col-lg-6 mb-4">
    <?php echo $no.".Crawl results from: ";?>
    <span class="badge badge-pill badge-info"><?php echo $webName[$i];?></span>
    <span> <a href="<?php echo base_url();?>csv/<?php echo $this->session->userdata('id')."_".$cleanHTTP[$i];?>.csv" class="btn btn-primary btn-sm mb-2" role="button">Export as CSV</a></span>
    <div class="col-lg-12 skrol">
        <table border="1px" style="margin-bottom:5px;width:100%">
            <thead>
                <tr>
                    <td>No.</td>
                    <td>Label</td>
                </tr>
            </thead>
            <tbody>
            <?php
            $menuNo = 0;
            foreach($hsl[$i] as $data){
                $menuNo++;
            ?>
            <tr>
                <td><?php echo $menuNo;?></td>
                <td><?php echo $data[0];?></td>
            </tr>
        <?php
            }
            ?>
            </tbody>
        </table>
    </div>
</div>
    <?php
    }
    ?>
    </div>
</div>
