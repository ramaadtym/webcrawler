     <div class="container upload-box ">
         <h3>
            <span class="logo mx-auto mb-3 rounded-circle px-auto py-3">
              <i class="fas fa-sitemap"></i>
            </span>
          Web Menu Crawler
        </h3>
         <div class="row">
             <div class="col-lg-12">
                 <h3>What is Web Menu Crawler? </h3>
                 <p>
                     Web Menu Crawler is an application to know the menus are often used on a website.
                     Web crawler will compare some websites entered by the user then will calculate the appearance of the word in the menu among the comparison websites using certain calculations.
                     <strong> The most common word is the recommended word to use on the website to be developed.</strong>

                 </p>
             </div>
         </div>
           <div class="row">
              <div class="col-lg-12">
                <h3>How to use?</h3>
                <p>There are 2 Options to use this aplication.

                </p>
                <?php if($this->session->flashdata('oops')){?>
                  <div class="alert alert-danger" role="alert">
                   <?php echo $this->session->flashdata('oops');?>
                  </div>
                <?php } ;?>
              </div>
           </div>
           <div class="row">
             <div class="col-lg-5 jrk ">
              <div class="opt1">
                A
              </div>
              <div class="col-lg-12 px-5 py-3 info">
              <div class="row">
                       <div class="col-lg-9">
                           <h4>Input Link</h4>
                           <small>enter the link you want to crawl</small>
                           <p class="mt-2">example: http://telkomuniversity.ac.id</p>
                       </div>
                       <div class="col-lg-3">
                           <div class="btn-group" role="group" aria-label="Basic example">
                               <button class="btn btn-outline-info btn-sm float-right" onclick="addLinkForm()" title="Add Link"><i class="fas fa-plus"></i></button>
                               <button class="btn btn-outline-info btn-sm float-right" onclick="delLinkForm()" title="Remove Link"><i class="fas fa-trash"></i></button>
                           </div>

                       </div>
                   </div>
                   <form class="mt-3" method="post"  action="<?php echo base_url();?>Crawl/crawl">
                   <div class="form-row" id="formLink">
                   <div class="col-12">
                     <input type="text" class="form-control" name="crwl[]" placeholder="Enter Link 1" required>
                   </div>
                   <div class="col-12 mt-2">
                     <input type="text" class="form-control" name="crwl[]" placeholder="Enter Link 2" required>
                   </div>
                   </div>
                    <button type="submit" class="btn btn-primary btn-block mt-3">GO</button>
                 </form>

                   
               </div>
            </div>
            <div class="col-lg-1 align-self-center">
              <div class="or_bg mx-auto  px-1 rounded-circle text-center">
                <p class="or_opt py-1 px-1">OR</p>
              </div>
            </div>

             <div class="col-lg-5 jrk">
              <div class="opt2">
                B
              </div>
               <div class="col-lg-12 px-4 py-5 info ">
               <h4 class="mb-3">Upload Files</h4>
                  <div class="mb-2 text-center press" id="hiddenPress">Press <strong>CTRL + click</strong> to add multiple files!</div>
                  <!-- Upload with button-->
                   <div>
                     <form method="post"  action="<?php echo base_url();?>User/uploads"  enctype="multipart/form-data">
                      <!-- <input type="file" name="file" class="form-control"> -->
<!--                         class="getNamaFile"-->
                     <div id="yourFiles"></div>
                      <div id="insertClassFile">
                          <span id="namafile"></span>
                      </div>
                      <label class="btn btn-primary btn-block">
                           <!--<input type="file" name="file" style="display: none;" onchange="$('#namafile').text('File: '+ this.files[0].name);" multiple> Upload Files-->
                            <input type="file" name="file[]" style="display: none;" id="ungh" accept=".csv" onchange="ambilNama()" multiple> Add Files
                       </label>
                      <button type="submit" class="btn btn-block btn-primary" id="upload_btn">Submit</button>
                    </form>
                   </div>
                   <!--End Upload with button-->
                
               </div>

             </div>
         <!--   <div class="col-lg-1 align-self-center text-center">
             <div class="or_bg mx-auto  px-1 rounded-circle text-center">
               <p class="or_opt py-1 px-1">OR</p>
             </div>
            </div>
            <div class="col-lg-3 jrk">
              <div class="opt3">
                C
              </div>
               <div class="col-lg-12 px-4 py-5 info ">
                    <h4 class="mb-3">Download Template</h4>
                    <p class="mb-4">You can download the CSV template to customize it</p>
                    <a href="#" class="btn btn-success mb-4" role="button"><i class="fas fa-download"></i> Download Template</a>
               </div>

            </div>-->
      </div>
       <div class="row">
               <div class="col-lg-12"> <p class="text-center my-5 mx-auto"> Copyright &copy; 2018 &bull; Dev Hibah Team - Telkom University</p>
               </div>
           </div>
     </div>

     <div id="loading"></div>
     <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
     <script>
       $(document).ready(function(){
        // $("input[type=text]").focus(function(){
          // $("input[type=text]").val("http://");
        // });
      });
      
     function ambilNama(){
          var files = document.getElementById("ungh").files;
          var output = "";
          var html = "";
          console.log(files.length);
          $("#yourFiles").text("Your Files: ");
          $("#hiddenPress").hide();

          if(files.length > 4){
            $("#insertClassFile").addClass("getNamaFile");
          }
          else{
              $("#insertClassFile").css({"margin-bottom":"4%","padding" : "5px 0 0 14px"})
          }
          for(var i = 0;i < files.length;i++){
              output += files[i].name + "<br>";
              html+= $("#namafile").text(files[i].name);
          }
          $("#namafile").html(output);
      }
      var idName = 2;

      function addLinkForm(){
          if(idName <= 9){
              var noLink = idName+1;
              var newForm =
                  " <div class=\"col-12 mt-2\" id='formCrwl"+idName+"'>" +
                  "  <input type=\"text\" class=\"form-control\" placeholder='Enter Link "+noLink+"' name='crwl[]' required>" +
                  // "  <input type=\"text\" class=\"form-control\" name='crwl[]' required>" +
                  "</div>"
              $("#formLink").append(newForm);
              idName++;
          }
      }
      function delLinkForm(){
          idName--;
          var link = $("#formCrwl"+idName).remove();
          if(idName < 3){
              idName = 2
          }
//             link.remove();
      }
     </script>