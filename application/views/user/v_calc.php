
<div class="container">

    <div class="row mb-3 mt-3">
        <div class="col-lg-6">
            <h3>Levenshtein Result</h3>
        </div>
        <div class="col-lg-6">
            <div class="float-right">
                <!-- <div class="alert alert-warning" role="alert">
                    Your Levenshtein Result will be deleted within <span id="timer" style="font-weight:bold">05:00</span> minutes!
                </div> -->
            </div>
        </div>
    </div>

    <!--     <div id="ListDataLev">
    
        </div> -->

    <pre>
        <?php echo  $this->session->userdata('id');?>
    </pre>
    <div class="resultTable">
        <table id="result" class="cell-border" style="width:100%">
            <thead>
                <tr id='hulu'>
                    <th width="2%">No</th>
                    <th width="2%">Label</th>
                    <?php for ($i = 0; $i < sizeof($name); $i++): ?>
                        <th><?php echo $name[$i]; ?></th>
                    <?php endfor; ?>
                    <th>Average</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $no = 0;
                for ($p = 0; $p < sizeof($label['data']); $p++):
                    $no++;
                    $avg = 0;
                    ?>

                    <tr id=" . p . ">
                        <td><?php echo $no; ?></td>
                        <td><?php echo $label['data'][$p]; ?></td>
                        <?php for ($i = 0; $i < sizeof($name); $i++): ?>
                            <td><?php
                                $avg += $calc[$i][$p];
                                echo round($calc[$i][$p],2)."%";
                                ?></td>
    <?php endfor; ?>
                        <td><?php echo round($avg / sizeof($name),2)."%"; ?></td>

                    </tr>
<?php endfor; ?>

            </tbody>
        </table>
        <!-- <div class="col-lg-12">
    
            <div class="panel panel-color panel-custom">
    
                <div class="panel-body" id="listPembanding">
    
                </div>
                <div class="panel-body" id="listTabel">
    
                </div>
                <div class="panel-body" id="perbandinganAkhir">
    
                </div>
            </div>
        </div> -->

    </div>
</div>
<p id="list">

</p>



<script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js"></script>
<script src="https://code.jquery.com/jquery-3.3.1.js"></script> 
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
<!-- <script src="<?php echo base_url(); ?>assets/js/levein.js"></script> -->
<script>
$(document).ready(function () {
    document.title = 'Web Crawler - Levenshtein Result';
    $('#result').DataTable({
        "lengthMenu": [[20, 40, 50, -1], [20, 40, 50, "All"]],
        dom: 'lBfrtip',
        buttons: [
            {
                extend:    'excelHtml5',
                text:      'Export as Excel',
                filename:  'Excel Leveinshtein Result'
            },
            {
                extend:    'csvHtml5',
                text:      'Export as CSV',
                filename:  'CSV Leveinshtein Result'
            },
            {
                extend:    'pdfHtml5',
                text:      'Export as PDF',
                orientation: 'landscape',
                pageSize: 'A3',
                filename:  'PDF Leveinshtein Result'
            }
        ]
        // "bInfo": false,
        //    "bFilter": false,
        // "bPaginate": false,
        // "showNEntries": false
    });
});
// function startTimer(duration, display) {
//     var timer = duration, minutes, seconds;
//     setInterval(function () {
//         minutes = parseInt(timer / 60, 10);
//         seconds = parseInt(timer % 60, 10);

//         minutes = minutes < 10 ? "0" + minutes : minutes;
//         seconds = seconds < 10 ? "0" + seconds : seconds;

//         display.text(minutes + ":" + seconds);

//         if (--timer < 0) {
//             timer = 0;
//             window.location.replace("<?php echo base_url(); ?>");
//         }
//     }, 1000);
// }

// jQuery(function ($) {
//     var fiveMinutes = 1 * 5,
//         display = $('#timer');
//     startTimer(fiveMinutes, display);
// });
</script>
