  <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>Version</b> 2.3.0
        </div>
        <strong>Copyright &copy; 2014-2015 <a href="http://almsaeedstudio.com">Almsaeed Studio</a>.</strong> All rights reserved.
      </footer>

      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->

    <!-- jQuery 2.1.4 -->
    <script src="<?=base_url('assets/back-end/plugins/jQuery/jQuery-2.1.4.min.js');?>"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="<?=base_url('assets/back-end/bootstrap/js/bootstrap.min.js');?>"></script>
    <!-- DataTables -->
     <?php
      if (isset($add_js)):
        foreach ($add_js as $value):
      ?>
    <script src="<?=$value?>"></script>

    <?php
        endforeach;
      endif;
    ?>
    <script src="<?=base_url('assets/back-end/plugins/datatables/jquery.dataTables.min.js');?>"></script>
    <script src="<?=base_url('assets/back-end/plugins/datatables/dataTables.bootstrap.min.js');?>"></script>
    <!-- SlimScroll -->
    <script src="<?=base_url('assets/back-end/plugins/slimScroll/jquery.slimscroll.min.js');?>"></script>
    <!-- FastClick -->
    <script src="<?=base_url('assets/back-end/plugins/fastclick/fastclick.min.js');?>"></script>
    <!-- AdminLTE App -->
    <script src="<?=base_url('assets/back-end/dist/js/app.min.js');?>"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="<?=base_url('assets/back-end/dist/js/demo.js');?>"></script>

    <?php
        if (isset($opsional)) {
            foreach ($opsional as $value) {
              echo $value;
            }
        }
    ?>
    <!-- page script -->
  </body>
</html>
