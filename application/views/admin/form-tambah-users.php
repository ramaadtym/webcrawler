      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Form Tambah User
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Forms</a></li>
            <li class="active">Tambah User</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">

          <!-- SELECT2 EXAMPLE -->
          <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title">Input Data User</h3>
             
            </div><!-- /.box-header -->
            <div class="box-body">
              <div class="row">
                <form action="<?=base_url('index.php/User/proses_tambah/');?>" method="POST">
                <div class="col-md-6">
                   <div class="form-group">
                    <label>Username</label>
                      <input type="text" name="username" class="form-control" >
                  </div><!-- /.form-group -->

                   <div class="form-group">
                    <label>Password</label>
                      <input type="password" name="password" class="form-control" >
                  </div><!-- /.form-group -->
                    <div class="form-group">
                    <label>Email</label>
                      <input type="email" name="email" class="form-control" >
                  </div><!-- /.form-group -->
                   <div class="form-group">
                    <label>No Telp</label>
                      <input type="text" name="notelp" class="form-control" >
                  </div><!-- /.form-group -->
                </div><!-- /.col -->
                <div class="col-md-6">
                    <div class="form-group">
                    <label>Nama Perusahaan</label>
                      <input type="text" name="namaperusahaan" class="form-control" >
                  </div><!-- /.form-group -->
                  <div class="form-group">
                    <label>Alamat</label>
                      <textarea style="height:107px;" name="alamat" class="form-control"></textarea>
                  </div><!-- /.form-group -->
                  <div class="form-group">
                    <label>Jenis Akses</label>
                    <select class="form-control select2" name="jenisakses" style="width: 100%;">
                      <option value="User">User</option>
                     <option value="Admin">Admin</option>
                    </select>
                  </div><!-- /.form-group -->
                </div><!-- /.col -->
                <div class="col-lg-6 col-lg-offset-10">
                    <button type="submit" class="col-md-3 btn bg-navy btn-flat margin">Insert</button>
                </div>
                </form>
              </div><!-- /.row -->
            </div><!-- /.box-body -->
          </div><!-- /.box -->

         

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->