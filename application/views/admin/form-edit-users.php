      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Form Tambah User
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Forms</a></li>
            <li class="active">Tambah User</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">

          <!-- SELECT2 EXAMPLE -->
          <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title">Input Data User</h3>
             
            </div><!-- /.box-header -->
            <div class="box-body">
              <div class="row">
                <form action="<?=base_url('index.php/User/proses_update/'.$data_user->username);?>" method="POST">
                <div class="col-md-6">

                    <div class="form-group">
                    <label>Email</label>
                      <input type="email" name="email"  value="<?=$data_user->email?>" class="form-control" >
                  </div><!-- /.form-group -->
                   <div class="form-group">
                    <label>No Telp</label>
                      <input type="text" name="notelp"  value="<?=$data_user->notelp?>" class="form-control" >
                  </div><!-- /.form-group -->
                   <div class="form-group">
                    <label>Alamat</label>
                      <textarea style="height:107px;" name="alamat" class="form-control"><?=$data_user->alamat?></textarea>
                  </div><!-- /.form-group -->
                </div><!-- /.col -->
                <div class="col-md-6">
                    <div class="form-group">
                    <label>Nama Perusahaan</label>
                      <input type="text" name="namaperusahaan"  value="<?=$data_user->namaperusahaan?>" class="form-control" >
                  </div><!-- /.form-group -->
                 
                  <div class="form-group">
                    <label>Jenis Akses</label>
                    <select class="form-control select2" name="jenisakses" style="width: 100%;">
                      <?php
                        if ($data_user->jenisakses=='Admin') {
                          ?>
                          <option value="Admin">Admin</option>
                        <option value="User">User</option>

                          <?php
                        }
                        else
                        {
                          ?>
                        <option value="User">User</option>
                          <option value="Admin">Admin</option>

                        <?php
                        }

                      ?>
                      
                    </select>
                  </div><!-- /.form-group -->
                </div><!-- /.col -->
                <div class="col-lg-6 col-lg-offset-10">
                    <button type="submit" class="col-md-3 btn bg-navy btn-flat margin">Update</button>
                </div>
                </form>
              </div><!-- /.row -->
            </div><!-- /.box-body -->
          </div><!-- /.box -->

         

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->