     <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Data <?=$label['parent']?>
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?=$label['link_home']?>"><i class="fa fa-users"></i> <?=$label['home']?></a></li>
            <li><a href="<?=$label['link_parent']?>"><?=$label['parent']?></a></li>
            <li class="active"><?=$label['sub_parent']?></li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
             
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title"><a href="<?=base_url('index.php/user/tambah')?>"><button class="btn btn-default"><i class="fa fa-plus"></i></button></a></h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Username</th>
                        <th>Email</th>
                        <th>Alamat</th>
                        <th>No Telp</th>
                        <th>Nama Perusahaan</th>
                        <th>Akses</th>
                        <th>Aksi</th>
                      </tr>
                    </thead>
                    <tbody>
                        <?php
                            foreach ($data_user as $value) {
                                echo "
                                  <tr>
                                    <td>".$value->username."</td>
                                    <td>".$value->email."</td>
                                    <td>".$value->alamat."</td>
                                    <td>".$value->notelp."</td>
                                    <td>".$value->namaperusahaan."</td>
                                    <td>".$value->jenisakses."</td>
                                    <td>
                                      <a href='User/edit/".$value->username."'><i class='fa fa-edit'></i></a>&nbsp;
                                      <a href='User/delete/".$value->username."'><i class='fa fa-trash'></i></a>&nbsp;
                                    </td>
                                  </tr>
                                " ;
                            }
                        ?>
                    </tbody>
                    <tfoot>
                      <tr>
                          <th>Username</th>
                        <th>Email</th>
                        <th>Alamat</th>
                        <th>No Telp</th>
                        <th>Nama Perusahaan</th>
                        <th>Akses</th>
                        <th>Aksi</th>
                      </tr>
                    </tfoot>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->