<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Web Menu Crawler</title>

    <!-- Bootstrap -->
    <link href="<?php echo base_url();?>assets/css/bootstrap-4.0.0.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/dropzone.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/loading-bar.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/animate.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/main.css" rel="stylesheet">
    <!-- <link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet"> -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css"/>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css"/>
    <script src="<?php echo base_url();?>assets/js/vue/vue.min.js"></script>
    <script defer src="<?php echo base_url();?>assets/js/fontawesome/js/fontawesome-all.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Ubuntu" rel="stylesheet">
  </head>
  <body>
  <?php
      echo $konten;
  ?>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) --> 
    <!-- <script src="<?php echo base_url();?>assets/js/jquery-3.2.1.min.js"></script> -->
    <script src="<?php echo base_url();?>assets/js/dropzone.js"></script>
    <script src="<?php echo base_url();?>assets/js/loading-bar.js"></script>
    <script src="<?php echo base_url();?>assets/js/vue/datavue.js"></script>
    <!-- <script src="https://code.jquery.com/jquery-3.3.1.js"></script> -->
    <!-- <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script> -->

    <script>
//        var myDropzone = new Dropzone("div#myId", { url: "/file/post"});  

      Dropzone.options.myAwesomeDropzone ={
        url: "<?php echo base_url('User/import');?>",
        maxFilesize: 1,
        method: "post",
        paramName: "csvfile",
        accept: function(csvfile, done) {
          // done("SIP");
          alert("All files have uploaded ");
        }
      };
    </script>
    <!-- Include all compiled plugins (below), or include individual files as needed --> 
    <!--<script src="js/popper.min.js"></script>-->
    <!--<script src="js/bootstrap-4.0.0.js"></script>-->

  <script>
//       function ambilNama(){
//           var files = document.getElementById("ungh").files;
//           var output = "";
//           var html = "";
//           console.log(files.length);
//           $("#yourFiles").text("Your Files: ");
//           $("#hiddenPress").hide();

//           if(files.length > 4){
//             $("#insertClassFile").addClass("getNamaFile");
//           }
//           else{
//               $("#insertClassFile").css({"margin-bottom":"4%","padding" : "5px 0 0 14px"})
//           }
//           for(var i = 0;i < files.length;i++){
//               output += files[i].name + "<br>";
//               html+= $("#namafile").text(files[i].name);
//           }
//           $("#namafile").html(output);
//       }
//       var idName = 2;

//       function addLinkForm(){
//           if(idName <= 9){
//               var noLink = idName+1;
//               var newForm =
//                   " <div class=\"col-12 mt-2\" id='formCrwl"+idName+"'>" +
//                   "  <input type=\"text\" class=\"form-control\" placeholder='Enter Link "+noLink+"' name='crwl[]'>" +
//                   "</div>"
//               $("#formLink").append(newForm);
//               idName++;
//           }
//       }
//       function delLinkForm(){
//           idName--;
//           var link = $("#formCrwl"+idName).remove();
//           if(idName < 3){
//               idName = 2
//           }
// //             link.remove();
//       }
      $(document).ready(function(){
          //Callback handler for form submit event
          $("#wantUnggah").submit(function(e)
          {

              var formObj = $(this);
              var formURL = formObj.attr("action");
              var formData = new FormData(this);
              $.ajax({
                  url: formURL,
                  type: 'POST',
                  data:  formData,
                  contentType: false,
                  cache: false,
                  processData:false,
                  beforeSend: function (){
                      //$("#loading").show(1000).html("<img src='<?php echo base_url();?>assets/img/load.gif' height='50'>");
                      $('#loading').css('background-color','#17607d');
                      $('#loading').css('z-index','2');
                      $("#loading").show(1000).html("<div id=\"loading-center\">\n" +
                          " <div id=\"loading-center-absolute\">\n" +
                          "   <div class=\"object\" id=\"object_four\"></div>\n" +
                          "    <div class=\"object\" id=\"object_three\"></div>\n" +
                          "         <div class=\"object\" id=\"object_two\"></div>\n" +
                          "             <div class=\"object\" id=\"object_one\"></div>\n" +
                          "          </div>\n" +
                          "<div class=\"abc\"> Loading ...</div>\n"+
                          "     </div>");
//                         $("#loading").show();
                  },
                  success: function(data, textStatus, jqXHR){
//                         $("#result").html(data);
                      //$("#loading").hide();
                      window.location.href = "<?php echo base_url()?>User/viewData";


                  },
                  error: function(jqXHR, textStatus, errorThrown){
                  }
              });
              e.preventDefault(); //Prevent Default action.
//                 e.unbind();
          });
          $("#viaLink").submit(function(e)
          {

              var formObj = $(this);
              var formURL = formObj.attr("action");
              var formData = new FormData(this);
              $.ajax({
                  url: formURL,
                  type: 'POST',
                  data:  formData,
                  contentType: false,
                  cache: false,
                  processData:false,
                  beforeSend: function (){
                      //$("#loading").show(1000).html("<img src='<?php echo base_url();?>assets/img/load.gif' height='50'>");
                      $('#loading').css('background-color','#17607d');
                      $('#loading').css('z-index','2');
                      $("#loading").show(1000).html("<div id=\"loading-center\">\n" +
                          " <div id=\"loading-center-absolute\">\n" +
                          "   <div class=\"object\" id=\"object_four\"></div>\n" +
                          "    <div class=\"object\" id=\"object_three\"></div>\n" +
                          "         <div class=\"object\" id=\"object_two\"></div>\n" +
                          "             <div class=\"object\" id=\"object_one\"></div>\n" +
                          "          </div>\n" +
                          "<div class=\"abc\"> Loading ...</div>\n"+
                          "     </div>");
//                         $("#loading").show();
                  },
                  success: function(data, textStatus, jqXHR){
//                         $("#result").html(data);
//                         $("#loading").hide();
                      window.location.href = "<?php echo base_url();?>Crawl/asd";

                  },
                  error: function(jqXHR, textStatus, errorThrown){
                  }
              });
              e.preventDefault(); //Prevent Default action.
//                 e.unbind();
          });

      });
  </script>

  </body>
</html>
