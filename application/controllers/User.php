<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends OK_Controller {
  function __construct()
  {
    parent::__construct();
    $this->load->model('User_model');
    
  }


  function formUpload()
  {
      $this->load->view('user/header');
      $this->load->view('user/formUpload');
      $this->load->view('user/footer');

  }
  public function uploads(){
    //   error_reporting(0);
      
      $sesi = session_id();
      
      $getName = array();
      $resData = array();   
      $removeDuplicate = array();   
      $rebuild = array();  
      $clearDuplicate = array();
    
      $fileNames = array();
      $fileLoad = array();
    
      
      $tot_file = count($_FILES['file']['tmp_name']);
      echo pathinfo($_FILES['file']['name'][0],PATHINFO_EXTENSION);
    //   print_r(pathinfo($_FILES['file']['name'][0],PATHINFO_EXTENSION));

      if($tot_file > 1){
        if($tot_file <= 10)  {
            for($i = 0;$i < $tot_file; $i++):
                // if(pathinfo($_FILES['file']['name'][0],PATHINFO_EXTENSION) == "csv"){

                // }
                $filename = $_FILES["file"]["tmp_name"];
                array_push($resData,$this->upload($filename[$i], $tot_file,$sesi));
                
                $withEXT = $_FILES['file']['name'][$i];
                $uploadwithEXT = $sesi."_".$_FILES['file']['name'][$i];
                $fileNames[$i] = basename(str_replace($sesi."_","",$uploadwithEXT),".csv");
                $fileLoad[$i] = $uploadwithEXT;
                $this->session->set_userdata('filecsv',$fileNames);
                $this->session->set_userdata('fileLoad',$fileLoad);
                
                //Buat CSV 1 file 1
                $output = fopen("csv/$uploadwithEXT", "w");
                $header = array('label');
                fputcsv($output, $header);
                //   Masukkan data dari array ke csv
                foreach($resData[$i] as $row){
                    fputcsv($output, $row);
                }  
                $withoutEXT  = basename($withEXT,'.csv');
                array_push($getName,$withEXT);
                //   echo "<pre>";
                //     print_r($resData);
                //   echo"</pre>";
                //remove duplicate
                $removeDuplicate = array_unique(array_column($resData[$i],'label'));
                //rebuild setelah removeduplicate
                $rebuild[] = array_intersect_key($resData[$i], $removeDuplicate);
                $label[] = array_column($resData[$i],'label');
                
            endfor;

            

            //Buat CSV untuk gabungan
                $output = fopen("csv/merge/$sesi.csv", "w");
                $header = array('label');
                fputcsv($output, $header);

                for($i = 0;$i < $tot_file; $i++):
                    $mergeRemDuplicate[$i] = array_merge($label[$i]);            
                    // $mergeRemDuplicate +=  $label[$i];            
                    // $rebuildMerge = array_intersect_key($resData[$i], $unique);

                    //   Masukkan data dari array ke csv gabungan
                    foreach($mergeRemDuplicate[$i] as $row){
                        $res[] = $row;
                        $unique = array_values(array_unique($res));
                    }
                endfor;
                // echo"<pre>"; print_r($this->session->userdata('filecsv'));echo"</pre>";
                
            
                    fputcsv($output, $unique,"\n");

                    fclose($output);

            //Passing ke halaman selanjutnya
            
            $this->session->set_userdata('id',$sesi);
            $id = $this->session->userdata('id');
            //   echo $sesi;

            $data = array(
                'sesi' => $sesi,
                'webName' => $getName,
                'hsl' => array_values($rebuild),
                'link' => $tot_file
            );
            $this->utama('user/v_stepdua_file',$data);
            }
            else{
                $this->session->set_flashdata('oops','sorry, maximum file uploaded = 10 files!');
                redirect('/');
            }
        }
        else{
            $this->session->set_flashdata('oops','sorry, minimum file uploaded = 2 files!');
            redirect('/');
        }
    }  
private function upload($files,$count,$sesi){

        $getData = array();
        //for($i = 0;$i < $count; $i++):
        if($_FILES["file"]["size"] > 0)
        {

             $file = fopen($files, "r");
             $baris = 0;
             while (($importdata = fgetcsv($file, 20000, ";")) !== FALSE)
             {
                $data = array(
                //    'level'=> "Global",
                   'label' => $importdata[0]

                );
                if($baris > 0){  //jika dimulai dari baris kedua
                //    $this->User_model->insert($data);
                    array_push($getData,$data);
                }
                $baris++;
             }
             fclose($file);
        }
        else{
             echo "Gagal";

        }

       // endfor;
        return $getData;
  }

  

}
