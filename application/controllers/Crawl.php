<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Crawl extends OK_Controller {
    function __construct()
    {
        parent::__construct();
        $this->load->model('User_model');
    }

    public $sesi;
    public function crawl(){
        $crawl= $this->input->post('crwl[]');
        $hasil_crawl = array();
        $fileLoad = array();
        $webName = array();
        // $merge = array();
        $removeDuplicate = array();   
        $res = array();
        $sesi = session_id();
        
        $this->session->set_userdata('id',$sesi);
        $tot_link = count($crawl);
        // echo "JUM FILE". $tot_link;
     
            for($i = 0; $i < $tot_link; $i++){ 
                    $webName[] = str_replace("/","",substr($crawl[$i],7));
                    $getOne[] = $sesi."_".$webName[$i].".csv";
                    array_push($hasil_crawl,$this->Crawling($crawl[$i]));


                    //Buat CSV 1-1 
                    $output = fopen("csv/".$sesi."_".$webName[$i].".csv", "w");
                    $header = array('label');
                    fputcsv($output, $header);
                    
                    foreach($hasil_crawl[$i] as $row){

                        fputcsv($output, $row);
                    }  
                    $label[] = $hasil_crawl[$i];
                
            }
            // echo"<pre>"; print_r(str_replace("/","",$webName));echo"</pre>";

            // print_r($getOne);
            $this->session->set_userdata('fileLoad',$getOne);
            $this->session->set_userdata('filecsv',$webName);


            //Buat CSV untuk gabungan
            $output = fopen("csv/merge/$sesi.csv", "w");
            $header = array('label');
            fputcsv($output, $header);
            for($i = 0; $i < $tot_link; $i++){
                $merge[] = array_merge($label[$i]);
                foreach($merge[$i] as $row){
                    $unique = array_unique($row);
                    fputcsv($output, $unique);
                }
                // fputcsv($output, $merge[$i]);
            }
        
            fclose($output);
        

            //Passing ke halaman selanjutnya

            $id = $this->session->userdata("sesi");
            $data = array(
                'webName' => $crawl,
                'cleanHTTP' => $webName,
                'hsl' => $hasil_crawl,
                'link' => sizeof($crawl)
            );
            $this->utama('user/v_stepdua',$data);
    /*        echo "<pre>";
            print_r($hasil_crawl);
            echo "</pre>";*/
        }

      private function Crawling($crawl)  {
        // error_reporting(0);
        $file = $crawl;
        $doc = new DOMDocument();
        $doc->strictErrorChecking = false;
        $doc->recover=true;
        libxml_use_internal_errors(true);
        
        if ($doc->loadHTMLFile($file) == false) {

           if(libxml_get_errors()){
               print_r(libxml_get_errors());
               $errors =libxml_get_errors();
               foreach($errors as $error){
                   $psn = str_replace("failed to load external entity","", $error->message);
               }
            $this->session->set_flashdata('oops','Sorry, there was an error writing the url'.$psn.'.Make sure the writing of the URL is correct or using the http: // or https: // protocol');
            redirect('/');
           }else{
                $curl = curl_init($file);
            if(empty($curl)){
                 echo "Hai";
            }
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
            $htmlContent = curl_exec($curl);
            curl_close($curl);
    
            $doc->loadHTML($htmlContent);
    
           }
           
        }

        $xpath = new DOMXpath($doc);
        foreach ($xpath->query('//ul/li/a//text()') as $textNode) {
            $result[] = $textNode->nodeValue;
            $result = preg_replace("/[^\w \! \` \\ \~ \@ \# \$ \% \^ \& \* \( \) \+ \- \= \| \} \; \: \? \> \< \/ \. \, \[ \] ]/", " ", $result);
            $result = array_map('trim', $result);
            $result = str_replace("]", "", $result);
            $result = str_replace("}", "", $result);
            $result = str_replace(">", "", $result);

            $length = count($result);
		    for ($i = 0; $i < $length; $i++) {
                $karakterpertama = substr("$result[$i]", 0, 1); 
                if (preg_match("/^(('[\w-\s]+')|([\w-]+(?:\.[\w-]+)*)|('[\w-\s]+')([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/", $result[$i])) {
                    $result[$i] = str_replace($result[$i], "", $result[$i]);
                };
    
                if (preg_match("/[0-9 \! \` \\ \~ \@ \# \$ \% \^ \& \* \( \) \+ \- \= \| \} \{ \] \[ \; \: \? \> \< \/ \. \, \[ \] ]/", $karakterpertama)) {
                    $result[$i] = str_replace($result[$i], "", $result[$i]);
                };
                if (preg_match("/^((0?[13578]|10|12)(-|\/)(([1-9])|(0[1-9])|([12])([0-9]?)|(3[01]?))(-|\/)((19)([2-9])(\d{1})|(20)([01])(\d{1})|([8901])(\d{1}))|(0?[2469]|11)(-|\/)(([1-9])|(0[1-9])|([12])([0-9]?)|(3[0]?))(-|\/)((19)([2-9])(\d{1})|(20)([01])(\d{1})|([8901])(\d{1})))$/", $karakterpertama)) {
                    $result[$i] = str_replace($result[$i], "", $result[$i]);
                };
    
    
                if (strlen($result[$i])<2) {
                    $result[$i] = str_replace($result[$i], "", $result[$i]);
                };
                if (strlen($result[$i])>43) {
                    $result[$i] = str_replace($result[$i], '', $result[$i]);
                };
    
                if (stripos($result[$i], 'Comment') !== false ) {
                      $result[$i] = str_replace($result[$i], "", $result[$i]);
                };
                if (stripos($result[$i], 'snippet') !== false ) {
                      $result[$i] = str_replace($result[$i], "", $result[$i]);
                };
                if (stripos($result[$i], 'klik') !== false ) {
                      $result[$i] = str_replace($result[$i], "", $result[$i]);
                };
                if (stripos($result[$i], 'posted') !== false ) {
                      $result[$i] = str_replace($result[$i], "", $result[$i]);
                };
                if (stripos($result[$i], '[') !== false ) {
                      $result[$i] = str_replace($result[$i], "", $result[$i]);
                };
                if (stripos($result[$i], '{') !== false ) {
                      $result[$i] = str_replace($result[$i], "", $result[$i]);
                };
                if (stripos($result[$i], '<') !== false ) {
                      $result[$i] = str_replace($result[$i], "", $result[$i]);
                };
                if (strpos($result[$i], '...') !== false ) {
                      $result[$i] = str_replace($result[$i], "", $result[$i]);
                };
            }
            $result = array_filter(array_map('trim', $result));
      }
      foreach ($xpath->query('//div//a//text()') as $textNode) {
        $result[] = $textNode->nodeValue;
        $result = preg_replace("/[^\w \! \` \\ \~ \@ \# \$ \% \^ \& \* \( \) \+ \- \= \| \} \] \[ \; \: \? \> \< \/ \. \,]/", " ", $result);
        $result = array_map('trim', $result);
	    $result = str_replace("]", "", $result);
	    $result = str_replace("}", "", $result);
        $result = str_replace(">", "", $result);
        
        $length = count($result);
        for ($i = 0; $i < $length; $i++) {
            $karakterpertama = substr("$result[$i]", 0, 1); 
            if (preg_match("/^(('[\w-\s]+')|([\w-]+(?:\.[\w-]+)*)|('[\w-\s]+')([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/", $result[$i])) {
				$result[$i] = preg_replace($result[$i], "", $result[$i]);
			};

			if (preg_match("/[0-9 \! \` \\ \~ \@ \# \$ \% \^ \& \* \( \) \+ \- \= \| \} \{ \] \[ \; \: \? \> \< \/ \. \, \[ \] ]/", $karakterpertama)) {
				$result[$i] = str_replace($result[$i], "", $result[$i]);
			};
			if (preg_match("/^((0?[13578]|10|12)(-|\/)(([1-9])|(0[1-9])|([12])([0-9]?)|(3[01]?))(-|\/)((19)([2-9])(\d{1})|(20)([01])(\d{1})|([8901])(\d{1}))|(0?[2469]|11)(-|\/)(([1-9])|(0[1-9])|([12])([0-9]?)|(3[0]?))(-|\/)((19)([2-9])(\d{1})|(20)([01])(\d{1})|([8901])(\d{1})))$/", $karakterpertama)) {
				$result[$i] = preg_replace($result[$i], "", $result[$i]);
			};


			if (strlen($result[$i])<2) {
				$result[$i] = str_replace($result[$i], "", $result[$i]);
			};
			if (strlen($result[$i])>43) {
				$result[$i] = str_replace($result[$i], "", $result[$i]);
			};

			if (stripos($result[$i], 'Comment') !== false ) {
		  		$result[$i] = str_replace($result[$i], "", $result[$i]);
			};
			if (stripos($result[$i], 'snippet') !== false ) {
		  		$result[$i] = str_replace($result[$i], "", $result[$i]);
			};
			if (stripos($result[$i], 'klik') !== false ) {
		  		$result[$i] = str_replace($result[$i], "", $result[$i]);
			};
			if (stripos($result[$i], 'posted') !== false ) {
		  		$result[$i] = str_replace($result[$i], "", $result[$i]);
			};
			if (stripos($result[$i], '[') !== false ) {
		  		$result[$i] = str_replace($result[$i], "", $result[$i]);
			};
			if (stripos($result[$i], '{') !== false ) {
		  		$result[$i] = str_replace($result[$i], "", $result[$i]);
			};
			if (stripos($result[$i], '<') !== false ) {
		  		$result[$i] = str_replace($result[$i], "", $result[$i]);
			};
			if (strpos($result[$i], '...') !== false ) {
		  		$result[$i] = str_replace($result[$i], "", $result[$i]);
			};
        }
        $result = array_filter(array_map('trim', $result));
      }
      foreach ($xpath->query('//div/select//text()') as $textNode) {
        $result[] = $textNode->nodeValue;
        $result = preg_replace("/[^\w \! \` \\ \~ \@ \# \$ \% \^ \& \* \( \) \+ \- \= \| \} \] \[ \; \: \? \> \< \/ \. \,]/", " ", $result);
        $result = array_map('trim', $result);
	    $result = str_replace("]", "", $result);
	    $result = str_replace("}", "", $result);
	    $result = str_replace(">", "", $result);

        $length = count($result);
		for ($i = 0; $i < $length; $i++) {
            $karakterpertama = substr("$result[$i]", 0, 1); 
            if (preg_match("/^(('[\w-\s]+')|([\w-]+(?:\.[\w-]+)*)|('[\w-\s]+')([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/", $result[$i])) {
				$result[$i] = preg_replace($result[$i], "", $result[$i]);
			};

			if (preg_match("/[0-9 \! \` \\ \~ \@ \# \$ \% \^ \& \* \( \) \+ \- \= \| \} \{ \] \[ \; \: \? \> \< \/ \. \, \[ \] ]/", $karakterpertama)) {
				$result[$i] = preg_replace($result[$i], "", $result[$i]);
			};
			if (preg_match("/^((0?[13578]|10|12)(-|\/)(([1-9])|(0[1-9])|([12])([0-9]?)|(3[01]?))(-|\/)((19)([2-9])(\d{1})|(20)([01])(\d{1})|([8901])(\d{1}))|(0?[2469]|11)(-|\/)(([1-9])|(0[1-9])|([12])([0-9]?)|(3[0]?))(-|\/)((19)([2-9])(\d{1})|(20)([01])(\d{1})|([8901])(\d{1})))$/", $karakterpertama)) {
				$result[$i] = preg_replace($result[$i], "", $result[$i]);
			};


			if (strlen($result[$i])<2) {
				$result[$i] = preg_replace($result[$i], "", $result[$i]);
			};
			if (strlen($result[$i])>43) {
				$result[$i] = preg_replace($result[$i], "", $result[$i]);
			};

			if (stripos($result[$i], 'Comment') !== false ) {
		  		$result[$i] = preg_replace($result[$i], "", $result[$i]);
			};
			if (stripos($result[$i], 'snippet') !== false ) {
		  		$result[$i] = preg_replace($result[$i], "", $result[$i]);
			};
			if (stripos($result[$i], 'klik') !== false ) {
		  		$result[$i] = preg_replace($result[$i], "", $result[$i]);
			};
			if (stripos($result[$i], 'posted') !== false ) {
		  		$result[$i] = preg_replace($result[$i], "", $result[$i]);
			};
			if (stripos($result[$i], '[') !== false ) {
		  		$result[$i] = preg_replace($result[$i], "", $result[$i]);
			};
			if (stripos($result[$i], '{') !== false ) {
		  		$result[$i] = preg_replace($result[$i], "", $result[$i]);
			};
			if (stripos($result[$i], '<') !== false ) {
		  		$result[$i] = preg_replace($result[$i], "", $result[$i]);
			};
			if (strpos($result[$i], '...') !== false ) {
		  		$result[$i] = preg_replace($result[$i], "", $result[$i]);
			};
        }
        $result = array_filter(array_map('trim', $result));
      }
       //sesudah
	    foreach ($xpath->query('//div[contains(@class, "quick") or contains(@id, "top")]//text()') as $textNode) {
            $result[] = $textNode->nodeValue;
            $result = preg_replace("/[^\w \! \` \\ \~ \@ \# \$ \% \^ \& \* \( \) \+ \- \= \| \} \] \[ \; \: \? \> \< \/ \. \, ]/", " ", $result);
            $result = array_map('trim', $result);
            $result = str_replace("]", "", $result);
            $result = str_replace("}", "", $result);
            $result = str_replace(">", "", $result);

            $length = count($result);
            for ($i = 0; $i < $length; $i++) {
                $karakterpertama = substr("$result[$i]", 0, 1); 
                if (preg_match("/^(('[\w-\s]+')|([\w-]+(?:\.[\w-]+)*)|('[\w-\s]+')([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/", $result[$i])) {
                    $result[$i] = preg_replace($result[$i], "", $result[$i]);
                };
    
                if (preg_match("/[0-9 \! \` \\ \~ \@ \# \$ \% \^ \& \* \( \) \+ \- \= \| \} \{ \] \[ \; \: \? \> \< \/ \. \, \[ \] ]/", $karakterpertama)) {
                    $result[$i] = preg_replace($result[$i], "", $result[$i]);
                };
                if (preg_match("/^((0?[13578]|10|12)(-|\/)(([1-9])|(0[1-9])|([12])([0-9]?)|(3[01]?))(-|\/)((19)([2-9])(\d{1})|(20)([01])(\d{1})|([8901])(\d{1}))|(0?[2469]|11)(-|\/)(([1-9])|(0[1-9])|([12])([0-9]?)|(3[0]?))(-|\/)((19)([2-9])(\d{1})|(20)([01])(\d{1})|([8901])(\d{1})))$/", $karakterpertama)) {
                    $result[$i] = preg_replace($result[$i], "", $result[$i]);
                };
    
    
                if (strlen($result[$i])<2) {
                    $result[$i] = str_replace($result[$i], "", $result[$i]);
                };
                if (strlen($result[$i])>43) {
                    $result[$i] = str_replace($result[$i], "", $result[$i]);
                };
    
                if (stripos($result[$i], 'Comment') !== false ) {
                      $result[$i] = str_replace($result[$i], "", $result[$i]);
                };
                if (stripos($result[$i], 'snippet') !== false ) {
                      $result[$i] = str_replace($result[$i], "", $result[$i]);
                };
                if (stripos($result[$i], 'klik') !== false ) {
                      $result[$i] = str_replace($result[$i], "", $result[$i]);
                };
                if (stripos($result[$i], 'posted') !== false ) {
                      $result[$i] = str_replace($result[$i], "", $result[$i]);
                };
                if (stripos($result[$i], '[') !== false ) {
                      $result[$i] = str_replace($result[$i], "", $result[$i]);
                };
                if (stripos($result[$i], '{') !== false ) {
                      $result[$i] = str_replace($result[$i], "", $result[$i]);
                };
                if (stripos($result[$i], '<') !== false ) {
                      $result[$i] = str_replace($result[$i], "", $result[$i]);
                };
                if (strpos($result[$i], '...') !== false ) {
                      $result[$i] = str_replace($result[$i], "", $result[$i]);
                };
            }
            $result = array_filter(array_map('trim', $result));
        }
        foreach ($xpath->query('//button[contains(@id, "search") or contains(@class, "quick")]//text()') as $textNode) {
            $result[] = $textNode->nodeValue;
            $result = preg_replace("/[^\w \! \` \\ \~ \@ \# \$ \% \^ \& \* \( \) \+ \- \= \| \} \] \[ \; \: \? \> \< \/ \. \,]/", " ", $result);
            $result = array_map('trim', $result);
            $result = str_replace("]", "", $result);
            $result = str_replace("}", "", $result);
            $result = str_replace(">", "", $result);

            $length = count($result);
		    for ($i = 0; $i < $length; $i++) {
                $karakterpertama = substr("$result[$i]", 0, 1); 
                if (preg_match("/^(('[\w-\s]+')|([\w-]+(?:\.[\w-]+)*)|('[\w-\s]+')([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/", $result[$i])) {
                    $result[$i] = preg_replace($result[$i], "", $result[$i]);
                };
    
                if (preg_match("/[0-9 \! \` \\ \~ \@ \# \$ \% \^ \& \* \( \) \+ \- \= \| \} \{ \] \[ \; \: \? \> \< \/ \. \, \[ \] ]/", $karakterpertama)) {
                    $result[$i] = preg_replace($result[$i], "", $result[$i]);
                };
                if (preg_match("/^((0?[13578]|10|12)(-|\/)(([1-9])|(0[1-9])|([12])([0-9]?)|(3[01]?))(-|\/)((19)([2-9])(\d{1})|(20)([01])(\d{1})|([8901])(\d{1}))|(0?[2469]|11)(-|\/)(([1-9])|(0[1-9])|([12])([0-9]?)|(3[0]?))(-|\/)((19)([2-9])(\d{1})|(20)([01])(\d{1})|([8901])(\d{1})))$/", $karakterpertama)) {
                    $result[$i] = preg_replace($result[$i], "", $result[$i]);
                };
    
    
                if (strlen($result[$i])<2) {
                    $result[$i] = preg_replace($result[$i], "", $result[$i]);
                };
                if (strlen($result[$i])>43) {
                    $result[$i] = preg_replace($result[$i], "", $result[$i]);
                };
    
                if (stripos($result[$i], 'Comment') !== false ) {
                      $result[$i] = preg_replace($result[$i], "", $result[$i]);
                };
                if (stripos($result[$i], 'snippet') !== false ) {
                      $result[$i] = preg_replace($result[$i], "", $result[$i]);
                };
                if (stripos($result[$i], 'klik') !== false ) {
                      $result[$i] = preg_replace($result[$i], "", $result[$i]);
                };
                if (stripos($result[$i], 'posted') !== false ) {
                      $result[$i] = preg_replace($result[$i], "", $result[$i]);
                };
                if (stripos($result[$i], '[') !== false ) {
                      $result[$i] = preg_replace($result[$i], "", $result[$i]);
                };
                if (stripos($result[$i], '{') !== false ) {
                      $result[$i] = preg_replace($result[$i], "", $result[$i]);
                };
                if (stripos($result[$i], '<') !== false ) {
                      $result[$i] = preg_replace($result[$i], "", $result[$i]);
                };
                if (strpos($result[$i], '...') !== false ) {
                      $result[$i] = preg_replace($result[$i], "", $result[$i]);
                };
            }
            $result = array_filter(array_map('trim', $result));
        }
  
        foreach ($result as $a) {

            $hasil[] = $a;
            $hasil = array_unique($hasil);
                
    
            
        }
            //INI MENGGABUNGKAN HASIL ARRAY+SIMBOL DENGAN PEMISAH %
        $fix = (implode('%', $hasil));
        //INI MEMBERSIHKAN SIMBOL2/EMOJI
        $fix2 = trim(preg_replace('/\s+/',' ', $fix));
        //INI MENGGABUNGKAN HASIL YANG SUDAH BERSIH
        $fix_array = array_chunk(explode('%', $fix2), 1);

        return $fix_array;
    }//End Function

    // private function Crawling($crawl){
    //     //SCRIPT WEB CRAWL PAKE DOMDOCUMENT BAWAAN PHP
    //     error_reporting(0);
    //     $file = $DOCUMENT_ROOT. $crawl;
    //     $doc = new DOMDocument();
    //     $doc->strictErrorChecking = false;
    //     $doc->recover=true;
    //     libxml_use_internal_errors(true);
    //     if ($doc->loadHTMLFile($file) == false) {
    //         $curl = curl_init($file);
    //         curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
    //         $htmlContent = curl_exec($curl);
    //         curl_close($curl);
    //         $doc->loadHTML($htmlContent);
    //     }

    //     //MEMBUAT SELECTOR DENGAN DOMXPATH
    //     $xpath = new DOMXpath($doc);

    //     //INI MENYELEKSI TAG A DI UL->LI
    //     foreach ($xpath->query('//*[contains(@class, "nav") or contains(@class, "menu") or
    //     contains (@class, "footer") or contains (@class, "main") or
    //     contains (@class, "header") or contains (@class, "bot") or
    //     contains (@class, "form")]//ul//li//a/text()') as $textNode) {
    //         $result[] = $textNode->nodeValue;
    //         $result = preg_replace("/[^0-9a-zA-Z_ -|:\.]/", " ", $result);
    //         $result = array_filter(array_map('trim', $result));
    //     }

    //     //INI MENYELEKSI TAG SPAN DI UL->LI->
    //     foreach ($xpath->query('//*[contains(@class, "nav") or contains(@class, "menu") or
    //     contains (@class, "footer") or contains (@class, "main") or
    //     contains (@class, "header") or contains (@class, "bot") or
    //     contains (@class, "form")]//ul//li//a//span/text()') as $textNode) {
    //         $result[] = $textNode->nodeValue;
    //         $result = preg_replace("/[^0-9a-zA-Z_ -|:\.]/", " ", $result);
    //         $result = array_filter(array_map('trim', $result));

    //     }

    //     //INI MENYELEKSI TAG A DI UL->LI
    //     foreach ($xpath->query('//*[contains(@id, "footer") or contains(@id, "main") ]//ul//li//a/text()') as $textNode) {
    //         $result[] = $textNode->nodeValue;
    //         $result = preg_replace("/[^0-9a-zA-Z_ -|:\.]/", " ", $result);
    //         $result = array_filter(array_map('trim', $result));

    //     }

    //     //INI MENYELEKSI TAG A
    //     foreach ($xpath->query('//*[contains(@id, "footer") or contains(@class, "quick") ]//a//text()') as $textNode) {
    //         $result[] = $textNode->nodeValue;
    //         $result = preg_replace("/[^0-9a-zA-Z_ -|:\.]/", " ", $result);
    //         $result = array_filter(array_map('trim', $result));

    //     }

    //     //INI MENYELEKSI TAG A KHUSUS WEB UPi
    //     foreach ($xpath->query('//*[@id="select2"]//text()') as $textNode) {
    //         $result[] = $textNode->nodeValue;
    //         $result = preg_replace("/[^0-9a-zA-Z_ -|:\.]/", " ", $result);
    //         $result = array_filter(array_map('trim', $result));

    //     }

    //     //INI MENGGABUNGKAN HASIL ARRAY DARI SELURUH SELECTOR
    //     foreach ($result as $a) {
    //         $hasil[] = $a;
    //         $hasil = array_unique($hasil);

    //     }

    //     //INI MENGGABUNGKAN HASIL ARRAY+SIMBOL DENGAN PEMISAH %
    //     $fix = (implode('%', $hasil));
    //     //INI MEMBERSIHKAN SIMBOL2/EMOJI
    //     $fix2 = trim(preg_replace('/\s+/',' ', $fix));
    //     //INI MENGGABUNGKAN HASIL YANG SUDAH BERSIH
    //     $fix_array = array_chunk(explode('%', $fix2), 1);

    //     return $fix_array;
    // }
   }