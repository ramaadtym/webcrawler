<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Delete extends OK_Controller {
  function __construct()
  {
    parent::__construct();
       
  }
  public function index(){
    $files = glob('./csv/*.csv'); // get all file names
    foreach($files as $file){ // iterate files
      if(is_file($file))
        unlink($file); // delete file
    }
    $files2 = glob('./csv/merge/*.csv');
    foreach($files2 as $file){ // iterate files
        if(is_file($file))
          unlink($file); // delete file
      }
  }
}