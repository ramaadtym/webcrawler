<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Calculation extends OK_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('csvreader');
    }
    public function index() {
        // session_start();
        // print_r($_SESSION);
        $file = "./csv/merge/" . $this->session->userdata('id') . ".csv";
        $data['label'] = $this->loadFileCombine($file);
//        echo"<pre>";
//        print_r($data['label']['data']);
//        echo"</pre>";
        $data['name'] = $this->session->userdata('filecsv');
        $p = $this->session->userdata('fileLoad');
        
        $data['calc'] = $this->loadDataLev($p, $data['label']['data']);
//        echo"<pre>";
//        print_r($data['calc']);
//        echo"</pre>";
//        die();

//        echo"<pre>";
//        print_r($this->session->userdata('filecsv'));
//        echo"</pre>";
//     echo"<pre>"; print_r($p);echo"</pre>"; die();


        $this->utama('user/v_calc', $data);
    }

    public function loadFileCombine($file) {

        // $file = "./csv/merge/1qmkl987makut6d1s6q01lrgoebmhn6d.csv";
        $jumlahData = 0;
        $data = array();


        $dirfile = file_get_contents($file);

        $result = $this->csvreader->parse_file($file);
        $temp = array();
        for ($z = 0; $z < count($result); $z++) {

            $temp[] = $result[($z + 1)]["label"];

            $jumlahData++;
        }

        $data['data'] = array_keys(array_flip($temp));
        sort($data['data']);
        return $data;
    }

    
    function loadDataLev($p, $mergemaster) {
        $file = "";
        $jumlahData = 0;
        $data = array();
        $listLevPerFile = array();
        $listKey = array();
        for ($x = 0; $x < count($p); $x++) {
           
            $result = $this->csvreader->parse_file("csv/" . $p[$x]);
            for ($z = 0; $z < count($result); $z++) {
                $data['csvData'][$x][$z] = $result[($z + 1)]["label"];
                $jumlahData++;
            }
        }

        for ($x = 0; $x < count($p); $x++) {
            for ($s = 0; $s < count($mergemaster); $s++) {

                $lev = 0;
                $max = 0;
                $avg = 0;

                for ($z = 0; $z < count($data['csvData'][$x]); $z++) {
                    $word = $data['csvData'][$x][$z];
                    // print_r($data['csvData'][$x][$s]['label']);
                    $lev = levenshtein($word, $mergemaster[$s]);
                    //    echo $lev."<br>";
                    $maxTemp = (100 / strlen($mergemaster[$s])) * (strlen($mergemaster[$s]) - $lev);

                    if ($maxTemp > $max) {
                        $max = $maxTemp;
                        // echo $max;
                    }
                    // $listLevPerFile[$x][$data['csvData'][$x][$z]['label']] = $max;
//                        $asd['label'] = $data['csvData'][$x][$z]['label'];
//                        $asd['levRes'] = $max;
                }
                $listLevPerFile[$x][$s] = $max;
//                $avg += $max;
                

                // print_r($listLevPerFile);
//                $listKey[] = $data['csvData'][$s];
            }
//            $listLevPerFile[$x]['avg'] = $avg / count($mergemaster);
        }

//        for ($i = 0; $i < count($p); $i++) {
//            $merger = array_merge($listKey[$i]);
//        }
//        $res['lev'] = $listLevPerFile;
        // $res['listKey'] = array_unique(array_column($merger,"label"));
        return $listLevPerFile;
//        echo json_encode($res);
    }

//     function loadDataLev() {
//         $file = "";
//         $jumlahData = 0;
//         $data = array();
//         $listLevPerFile = array();
//         $listKey = array();
//         $dataLabelMaster = $this->input->post('dataLabelMaster');
//         if ($dataLabelMaster != "") {
//             //$cmps = json_decode($_SESSION['campus'], TRUE);
//             for ($x = 0; $x < count($_FILES); $x++) {
//                 if (!empty($_FILES)) {
//                     $dirfile = file_get_contents($_FILES['file' . $x]['tmp_name']);
//                     //$dirfile = file_get_contents($cmps[$x].'_'.session_id());
//                     $type = $_FILES['file' . $x]['type'];
// //var_dump($_FILES['file'.$x]['tmp_name']); die();
//                     $file = $dirfile . "." . $type;
//                     if ($type == "application/vnd.ms-excel") {
//                         $this->load->library('csvreader');
//                         $result = $this->csvreader->parse_file($_FILES['file' . $x]['tmp_name']);
//                         for ($z = 0; $z < count($result); $z++) {
//                             $data['csvData'][$x][$z]["level"] = $result[($z + 1)]["level"];
//                             $data['csvData'][$x][$z]["label"] = $result[($z + 1)]["label"];
// //                        $data[] = $result[($z + 1)]["label"];
//                             $jumlahData++;
//                         }
//                     }
//                 }
//             }
// //            print_r($dataLabelMaster); die();
//             $dataLabelMaster = json_decode($dataLabelMaster, TRUE);
// //            echo count($dataLabelMaster['csvData']); die();
//             for ($x = 0; $x < count($_FILES); $x++) {
//                 for ($s = 0; $s < count($dataLabelMaster['csvData']); $s++) {
//                     $lev = 0;
//                     $max = 0;
//                     for ($z = 0; $z < count($data['csvData'][$x]); $z++) {
//                         $word = $data['csvData'][$x][$z]["label"];
//                         $lev = levenshtein($word, $dataLabelMaster['csvData'][$s]);
// //                        echo $lev."<br>";
//                         $maxTemp = (100 / strlen($dataLabelMaster['csvData'][$s])) * (strlen($dataLabelMaster['csvData'][$s]) - $lev);
//                         if ($maxTemp > $max) {
//                             $max = $maxTemp;
//                         }
//                     }
//                     $listLevPerFile[$x][$dataLabelMaster['csvData'][$s]] = $max;
//                     $listKey[] = $dataLabelMaster['csvData'][$s];
//                 }
//             }
//         }
//         $data['lev'] = $listLevPerFile;
// //       echo  count($listLevPerFile[0]); 
//         $data['listKey'] = array_unique($listKey);
// //        print_r($listLevPerFile);
// //        die();
// //        var_dump();
// //        die();
// //        $file = './kecil.csv';
// //        $this->load->library('csvreader');
// //        $result = $this->csvreader->parse_file($file);
// ////        print_r($result);
// //        $data = "";
// //        for ($x = 0; $x < count($result); $x++) {
// //            $data['csvData'][$x]["level"] = $result[($x + 1)]["level"];
// //            $data['csvData'][$x]["label"] = $result[($x + 1)]["label"];
// //        }
// //        $data['csvData'] = $result;
//         echo json_encode($data);
//     }
}
